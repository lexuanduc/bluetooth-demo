//  3-1.Wi-Fi無線接続

// ①ライブラリを読み込み
#include <WiFi.h>
#include <HTTPClient.h>

//HUMAX-70405-A
//NaG5NjA5XWR4d

// ③Wi-Fi設定
const char *ssid      = "CISCO-ee75f-24g";
const char *password  = "GXdSGH3J5JSSh";
const char URL[] = "http://192.168.0.26/PHP/Beacon/index.html";

IPAddress ip(192, 168, 0, 99);   // IPアドレス（本機が利用するIP）
IPAddress gateway(192, 168, 0, 1);  // デフォルトゲートウェイ
IPAddress subnet(255, 255, 255, 0); // サブネットマスク

void setup(void) {
  // ④Serial設定
  Serial.begin(115200);
  Serial.println("");

  // ⑥無線Wi-Fi接続
  WiFi.config( ip, gateway, subnet );
  WiFi.begin( ssid, password );
  // ⑦Wi-Fi接続処理（接続するまで無限ループ）
  while ( WiFi.status() != WL_CONNECTED ) {
    // ⑨１秒間Wait
    delay ( 1000 );
    Serial.print(" . ");
  }
  // ⑩Wi-Fi接続できたのでシリアルモニターにIPアドレス表示
  Serial.print ( "Wi-Fi Connected! IP address: " );
  Serial.println ( WiFi.localIP() );
}

// loop処理はありません
void loop(void){
  HTTPClient http;
  http.begin(URL);
  int httpCode = http.GET();

  Serial.printf("Response: %d", httpCode);
  Serial.println();
  if (httpCode == HTTP_CODE_OK) {
    String body = http.getString();
    Serial.println("Response Body: ");
    Serial.println(body);
  }
  delay(5000);
}
