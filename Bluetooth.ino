﻿/*
 * Copyright (c) 2016 Intel Corporation.  All rights reserved.
 * See the bottom of this file for the license terms.
 */
 
#include <CurieBLE.h>
 
BLEPeripheral blePeripheral;  // BLE Peripheral Device (the board you're programming)
// BLE LED Switch Characteristic - custom 128-bit UUID, read and writable by central
BLEUnsignedCharCharacteristic switchCharacteristic("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);

void setup() {
  Serial.begin(9600);
 
  // set advertised local name and service UUID:
  blePeripheral.setLocalName("Le Duc");
 
  // add service and characteristic:
  blePeripheral.addAttribute(switchCharacteristic);
 
  // begin advertising BLE service:
  blePeripheral.begin();
}

void loop() {

}